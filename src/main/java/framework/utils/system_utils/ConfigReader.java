package framework.utils.system_utils;

import framework.browser.BrowserType;
import org.yaml.snakeyaml.Yaml;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ConfigReader {

    private final static String PROPERTY_FILENAME = "config";
    private final static String DESIRED_CAPS_SETTINGS_FILENAME = "desired_capabilities.yaml";

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTY_FILENAME);

    public static String getPropertyValue(String key) {
        return resourceBundle.getString(key);
    }

    public static Map<String, String> getDesiredCapabilities(BrowserType browserType) {
        String targetKey = browserType.toString().toLowerCase();
        Yaml yaml = new Yaml();
        InputStream inputStream = ConfigReader.class
                .getClassLoader()
                .getResourceAsStream(DESIRED_CAPS_SETTINGS_FILENAME);

        Map<String, Map> allCapabilitiesMap = yaml.load(inputStream);
        Map<String, String> resultMap = allCapabilitiesMap.get(targetKey);
        return (resultMap != null ? resultMap : new HashMap<>());
    }
}
