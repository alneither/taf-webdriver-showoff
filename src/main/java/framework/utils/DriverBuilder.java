package framework.utils;

import framework.browser.BrowserType;
import framework.exception.UnsupportedBrowserTypeException;
import framework.utils.system_utils.ConfigReader;
import org.openqa.selenium.WebDriver;

public class DriverBuilder {

    private static final String DEFAULT_BROWSER_TYPE_KEY = "debug_browser_type";
    private static WebDriver driver = null;

    public static WebDriver getDriverByType(BrowserType browserType) {
        if(driver != null) {
            return driver;
        }
        switch (browserType) {
            case IE: {
                driver = configureAndGetIEdriver();
            }
            case MOZILLA: {
                driver = configureAndGetMozillaDriver();
            }
            default: {
                driver = configureAndGetChromeDriver();
            }
            return driver;
        }
    }

    private static WebDriver configureAndGetChromeDriver() {
        return driver;
    }

    private static WebDriver configureAndGetMozillaDriver() {
        return driver;
    }

    private static WebDriver configureAndGetIEdriver() {
        return driver;
    }

    public WebDriver getDriver(){
        if(driver != null) {
            return driver;
        }
        String type = ConfigReader.getPropertyValue(DEFAULT_BROWSER_TYPE_KEY);
        BrowserType browserType;
        try {
            browserType = BrowserType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new UnsupportedBrowserTypeException(type + " type of a browser doesn't supported!", e);
        }
        return getDriverByType(browserType);
    }
}
