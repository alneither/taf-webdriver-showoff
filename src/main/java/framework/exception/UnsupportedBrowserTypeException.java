package framework.exception;

public class UnsupportedBrowserTypeException extends RuntimeException {

    public UnsupportedBrowserTypeException(String message, IllegalArgumentException e) {
        super(message, e);
    }
}
