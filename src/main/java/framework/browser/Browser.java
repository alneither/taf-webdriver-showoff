package framework.browser;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Browser {

    private WebDriver webDriver;

    public void screenshot() {
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
