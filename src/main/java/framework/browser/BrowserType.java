package framework.browser;

public enum BrowserType {
    CHROME, MOZILLA, IE
}
